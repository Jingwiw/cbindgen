%global _enable_debug_packages %{nil}
%global debug_package          %{nil}
# Use hardening ldflags.
%global crate_name cbindgen
%global rustflags -Clink-arg=-Wl,-z,relro,-z,now
Name:           rust-%{crate_name}
Version:        0.20.0+git0
Release:        1.1
Summary:        A tool for generating C bindings from Rust code
License:        MPL-2.0
Group:          Development/Languages/Rust
URL:            https://crates.io/crates/cbindgen
Source0:        %{crate_name}-%{version}.tar.xz
Source1:        vendor.tar.xz
Source2:        cargo_config
Source99:       UPDATING.md
BuildRequires:  cargo >= 1.57
BuildRequires:  rust >= 1.57

%description
A tool for generating C bindings from Rust code.

%prep
%setup -q -T -b 0 -n %{crate_name}-%{version}
%setup -q -D -T -a 1 -n %{crate_name}-%{version}
mkdir cargo-home
cp %{SOURCE2} cargo-home/config

%build
# This should eventually migrate to distro policy
# Enable optimization, debuginfo, and link hardening.
export RUSTFLAGS="%{rustflags}"
export CARGO_HOME=`pwd`/cargo-home/

cargo build --release

%install
# rustflags must be exported again at install as cargo build will
# rebuild the project if it detects flags have changed (to none or other)
export RUSTFLAGS="%{rustflags}"
# install stage also requires re-export of 'cargo-home' or cargo
# will try to download source deps and rebuild
export CARGO_HOME=`pwd`/cargo-home/
# cargo install appends /bin to the path
cargo install --root=%{buildroot}%{_prefix} --path .
# remove spurious files
rm -f %{buildroot}%{_prefix}/.crates.toml
rm -f %{buildroot}%{_prefix}/.crates2.json

%files
%license LICENSE
%{_bindir}/cbindgen

%changelog
